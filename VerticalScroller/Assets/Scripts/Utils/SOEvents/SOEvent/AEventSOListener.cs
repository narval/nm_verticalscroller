﻿using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay.UI
{
    public abstract class AEventSOListener : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] protected SOEvent eventSO;

        protected virtual void Start()
        {
            eventSO.Event += OnEvent;
        }
        protected virtual void OnDestroy()
        {
            eventSO.Event -= OnEvent;
        }
        protected virtual void OnEnable()
        {
            eventSO.Event += OnEvent;
        }

        protected virtual void OnDisable()
        {
            eventSO.Event -= OnEvent;
        }


        protected abstract void OnEvent();
    }
}
