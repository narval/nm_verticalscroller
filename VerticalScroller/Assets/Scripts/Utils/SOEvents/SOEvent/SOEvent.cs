using System;
using UnityEngine;

namespace Inarval.VerticalScroller.Utils
{
    [CreateAssetMenu(fileName = "SOEvent", menuName = "Events/SOEvent")]
    public class SOEvent : ScriptableObject
    {
        private System.Action onEvent;

        public event Action Event
        {
            add
            {
                onEvent -= value;
                onEvent += value;
            }

            remove
            {
                onEvent -= value;
            }
        }

        public void Invoke()
        {
            onEvent?.Invoke();
        }
    }
}
