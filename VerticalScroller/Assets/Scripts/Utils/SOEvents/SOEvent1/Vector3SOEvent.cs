﻿using UnityEngine;

namespace Inarval.VerticalScroller.Utils
{
    [CreateAssetMenu(fileName = "Vector3SOEvent", menuName = "Events/SOEvent1/Vector3SOEvent")]
    public class Vector3SOEvent : ASOEvent1<Vector3> { }
}