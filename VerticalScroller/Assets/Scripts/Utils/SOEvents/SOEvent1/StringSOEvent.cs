﻿using UnityEngine;

namespace Inarval.VerticalScroller.Utils
{
    [CreateAssetMenu(fileName = "StringSOEvent", menuName = "Events/SOEvent1/StringSOEvent")]
    public class StringSOEvent : ASOEvent1<string> { }
}