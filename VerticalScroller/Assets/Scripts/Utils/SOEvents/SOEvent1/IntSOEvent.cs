﻿using UnityEngine;

namespace Inarval.VerticalScroller.Utils
{
    [CreateAssetMenu(fileName = "IntSOEvent", menuName = "Events/SOEvent1/IntSOEvent")]
    public class IntSOEvent : ASOEvent1<int> { }
}