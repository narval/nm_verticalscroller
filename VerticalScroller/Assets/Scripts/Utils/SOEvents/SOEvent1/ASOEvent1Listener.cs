﻿using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay.UI
{
    public abstract class ASOEvent1Listener<T> : MonoBehaviour
    {
        [Header("References")]
        [NoNull] [SerializeField] protected ASOEvent1<T> event1SO;

        protected virtual void Start()
        {
            event1SO.ValueEvent += OnValueChange;
        }
        protected virtual void OnDestroy()
        {
            event1SO.ValueEvent -= OnValueChange;
        }
        protected virtual void OnEnable()
        {
            event1SO.ValueEvent += OnValueChange;
        }

        protected virtual void OnDisable()
        {
            event1SO.ValueEvent -= OnValueChange;
        }


        protected abstract void OnValueChange(T value);
    }
}
