﻿using UnityEngine;

namespace Inarval.VerticalScroller.Utils
{
    [CreateAssetMenu(fileName = "BoolSOEvent", menuName = "Events/SOEvent1/BoolSOEvent")]
    public class BoolSOEvent : ASOEvent1<bool> { }
}