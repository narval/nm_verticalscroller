using UnityEngine;

namespace Inarval.VerticalScroller.Utils
{
    public abstract class ASOEvent1<T> : ScriptableObject
    {
        public System.Action<T> valueEvent;

        public event System.Action<T> ValueEvent
        {
            add
            {
                valueEvent -= value;
                valueEvent += value;
            }

            remove
            {
                valueEvent -= value;
            }
        }

        public void Invoke(T value)
        {
            valueEvent?.Invoke(value);
        }
    }
}