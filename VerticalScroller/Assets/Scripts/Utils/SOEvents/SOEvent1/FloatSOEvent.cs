﻿using UnityEngine;

namespace Inarval.VerticalScroller.Utils
{
    [CreateAssetMenu(fileName = "FloatSOEvent", menuName = "Events/SOEvent1/FloatSOEvent")]
    public class FloatSOEvent : ASOEvent1<float> { }
}