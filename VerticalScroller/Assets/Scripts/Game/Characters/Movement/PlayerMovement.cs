using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    public class PlayerMovement : CharacterMovement
    {
        [field: SerializeField]
        public bool CanMove { get; private set; } = true;

        protected override void Move()
        {
            if (!CanMove)
            {
                return;
            }

            Vector3 deltaPosition = nextDirection * speed * Time.deltaTime;
            Vector3 nextPosition = transform.position + deltaPosition;


            characterRigidbody.MovePosition(nextPosition);
        }
    }
}