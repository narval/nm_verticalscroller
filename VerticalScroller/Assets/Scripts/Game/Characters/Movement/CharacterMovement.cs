using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay
{
    public class CharacterMovement : MonoBehaviour
    {
        [Header("References")]
        [NoNull] [SerializeField] protected Rigidbody2D characterRigidbody;
        [NoNull] [SerializeField] protected ACharacterInput characterInput;

        [Header("Configs")]
        [NoNull] [SerializeField] protected CharacterConfigSO initialConfig;

        [Header("Custom")]
        [SerializeField] protected float speed = 1;

        protected Vector3 nextDirection;

        protected virtual void Start()
        {
            if (characterInput == null)
            {
                throw new System.Exception("No suitable Input Controller found");
            }

            SubscribeToInput();

            speed = initialConfig.initialSpeed;
            
        }

        private void FixedUpdate()
        {
            Move();
        }

        private void SubscribeToInput()
        {
            characterInput.InputDirectionTickEvent += UpdateNextDirection;
        }

        private void OnDestroy()
        {
            characterInput.InputDirectionTickEvent -= UpdateNextDirection;
        }

        protected virtual void UpdateNextDirection(Vector3 direction)
        {
            nextDirection = direction;            
        }

        protected virtual void Move ()
        {
            if (nextDirection != Vector3.zero)
            {
                Vector3 deltaPosition = nextDirection * speed * Time.fixedDeltaTime;
                Vector3 nextPosition = transform.position + deltaPosition;

                characterRigidbody.MovePosition(nextPosition);

            }
        }

    }
}
