using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay
{
    public class Bullet : MonoBehaviour
    {
        [Header("References")]
        [NoNull][SerializeField] protected Rigidbody2D rb;

        [Header("Custom")]
        [Min(0)][SerializeField] protected float speed;
        [SerializeField] private int damage; //TODO: Add a public function to modify the damage or add as a parameter of Shoot
        [SerializeField] protected string borderTag;

        public int Damage => damage;

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.CompareTag(borderTag))
            {
                gameObject.SetActive(false);
            }
        }

        public void Shoot (Vector3 direction)
        {
            Vector3 velocity = direction.normalized * speed;
            rb.velocity = velocity;
        }

        public void OnEnemyHit (Collisionable collisionable)
        {
            gameObject.SetActive(false);
        }
    }
}
