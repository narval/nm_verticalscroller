using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay
{
    public class BulletShooter : MonoBehaviour
    {
        [Header("References")]
        [NoNull][SerializeField] protected ACharacterInput characterInput;

        [Header("Custom")]
        [Min(0)][SerializeField] private float cadence = 1;
        [SerializeField] ObjectPoolManager.PoolType poolType;

        private float timer;


        void Start()
        {
            timer = 0;

            if (characterInput == null)
            {
                throw new System.Exception("No suitable Input Controller found");
            }

            characterInput.InputFireTickEvent += Shoot;
        }

        private void OnEnable()
        {
            characterInput.InputFireTickEvent += Shoot;
        }

        private void OnDisable()
        {
            characterInput.InputFireTickEvent -= Shoot;
        }

        private void OnDestroy()
        {
            characterInput.InputFireTickEvent -= Shoot;
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Shoot();
            }

            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
        }

        private void Shoot()
        {
            if (timer > 0)
            {
                //Can't shoot yet
                return;
            }
            timer = cadence;

            GameObject bulletGO = ObjectPoolManager.Instance.GetObject(poolType);
            if (bulletGO == null)
            {
                Debug.LogError("Insuficient objects in pool");
                //TODO:Should make this dynamic later
                return;
            }
            bulletGO.transform.position = transform.position;
            Bullet bullet = bulletGO.GetComponent<Bullet>();
            bullet.Shoot(transform.up);
        }        
    }
}
