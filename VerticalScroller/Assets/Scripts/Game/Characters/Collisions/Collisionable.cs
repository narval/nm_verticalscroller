using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay
{
    public class Collisionable : MonoBehaviour
    {
        [Header("Config")]
        [NoNull] [SerializeField] private CharacterConfigSO initialConfig;

        [Header("Custom")]
        [SerializeField] protected string bulletTag;
        [SerializeField] protected string enemyTag;
        [SerializeField] private int collisionDamage;

        protected System.Action<int> onHitEvent;

        public event System.Action<int> OnHitEvent
        {
            add
            {
                onHitEvent -= value;
                onHitEvent += value;
            }

            remove
            {
                onHitEvent -= value;
            }
        }

        private void Start()
        {
            collisionDamage = initialConfig.collisionDamage;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag(bulletTag))
            {
                Bullet bullet = collision.GetComponent<Bullet>();
                onHitEvent?.Invoke(bullet.Damage);
                bullet.OnEnemyHit(this);
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (!string.IsNullOrEmpty(enemyTag) && collision.collider.CompareTag(enemyTag))
            {
                Collisionable collisionable = collision.collider.GetComponent<Collisionable>();
                onHitEvent?.Invoke(collisionable.collisionDamage);
            }
        }
    }
}
