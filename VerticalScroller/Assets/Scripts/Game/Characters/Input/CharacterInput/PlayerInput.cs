namespace Inarval.VerticalScroller.Gameplay
{
    public class PlayerInput : ACharacterInput
    {
        protected override void Start()
        {
            //Add Logic to choose the current input controller, probably depending on the platform
#if UNITY_EDITOR || UNITY_STANDALONE
            currentInputController = new KeyboardInputController();
#endif
        }

    }
}
