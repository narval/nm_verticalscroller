using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    public abstract class ACharacterInput : MonoBehaviour
    {
        protected AInputController currentInputController;

        protected System.Action<Vector3> inputDirectionTickEvent;

        public event System.Action<Vector3> InputDirectionTickEvent
        {
            add
            {
                inputDirectionTickEvent -= value;
                inputDirectionTickEvent += value;
            }
            remove
            {
                inputDirectionTickEvent -= value;
            }
        }

        protected System.Action inputFireTickEvent;

        public event System.Action InputFireTickEvent
        {
            add
            {
                inputFireTickEvent -= value;
                inputFireTickEvent += value;
            }
            remove
            {
                inputFireTickEvent -= value;
            }
        }

        [Header("Custom")]
        [SerializeField] protected bool normalizeDirection = true;

        protected abstract void Start();

        protected virtual void Update()
        {
            if(currentInputController == null)
            {
                return;
            }

            Vector3 direction = currentInputController.GetDirection(this);
            bool firing = currentInputController.GetShooting(this);

            //Apply corrections to input
            if (normalizeDirection)
            {
                direction.Normalize();
            }

            //Invoke events
            inputDirectionTickEvent?.Invoke(direction);
            if (firing)
            {
                inputFireTickEvent?.Invoke();
            }
        }
    }
}
