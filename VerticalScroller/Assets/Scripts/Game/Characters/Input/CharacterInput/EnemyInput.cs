using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    public class EnemyInput : ACharacterInput
    {
        public enum enemyIAEnum { IA1 };
        [SerializeField] enemyIAEnum enemyIAType;

        protected override void Start()
        {
            //Logic to choose the current input controller
            currentInputController = enemyIAType switch
            {
                enemyIAEnum.IA1 => new EnemyIA1InputController(),
                _ => new EnemyIA1InputController(),
            };

            (currentInputController as AEnemyInputController).Init(transform, GameManager.Instance.Player.transform);
        }
    }
}
