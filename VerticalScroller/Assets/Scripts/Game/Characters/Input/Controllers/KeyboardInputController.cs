using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    public class KeyboardInputController : AInputController
    {
        const string HORIZONTAL_AXIS = "Horizontal";
        const string VERTICAL_AXIS = "Vertical";
        const string JUMP_AXIS = "Jump";

        public override Vector3 GetDirection(ACharacterInput manager)
        {
            return new Vector3()
            {
                x = Input.GetAxisRaw(HORIZONTAL_AXIS),
                y = Input.GetAxisRaw(VERTICAL_AXIS),
                z = 0,
            };
        }

        public override bool GetShooting(ACharacterInput manager)
        {
            return Input.GetAxisRaw(JUMP_AXIS) > 0;
        }
    }
}
