using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    public abstract class AInputController
    {
        protected Transform selfTransform;
        public abstract Vector3 GetDirection(ACharacterInput manager);
        public abstract bool GetShooting(ACharacterInput manager);
    }
}