﻿using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    public abstract class AEnemyInputController : AInputController
    {
        protected Transform targetTransform;

        public void Init(Transform selfTransform, Transform targetTransform)
        {
            this.selfTransform = selfTransform;
            this.targetTransform = targetTransform;
        }
    }
}
