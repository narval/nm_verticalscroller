﻿using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    public class EnemyIA1InputController : AEnemyInputController
    {        
        const float MIN_X_DISTANCE = 0.1f; //TODO: Should be in the enemy configuration object
        const float MIN_Y_DISTANCE = 7.0f; //TODO: Should be in the enemy configuration object

        public override Vector3 GetDirection(ACharacterInput manager)
        {
            Vector3 directionResult = new Vector3();

            float xDifference = targetTransform.transform.position.x - selfTransform.position.x;
            if (Mathf.Abs(xDifference) > MIN_X_DISTANCE)
            {
                directionResult.x = xDifference;
            }

            float yDifference = targetTransform.transform.position.y - selfTransform.position.y;
            if (Mathf.Abs(yDifference) > MIN_Y_DISTANCE)
            {
                directionResult.y = yDifference;
            }

            return directionResult;
        }

        public override bool GetShooting(ACharacterInput manager)
        {
            float xDifference = targetTransform.transform.position.x - selfTransform.position.x;
            return (Mathf.Abs(xDifference) <= MIN_X_DISTANCE);
        }
    }
}
