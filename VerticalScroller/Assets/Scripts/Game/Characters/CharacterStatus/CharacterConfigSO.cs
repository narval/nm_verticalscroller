using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay
{
    [CreateAssetMenu(fileName = "CharacterConfigSO", menuName = "Configurations/Characters/CharacterConfigSO")]
    public class CharacterConfigSO : ScriptableObject
    {
        public int maxHp;
        [Min(0)] public int initialSpeed;
        [Min(0)] public int collisionDamage;
        [Min(0)] public int scoreValue;        
    }
}
