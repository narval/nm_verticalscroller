﻿namespace Inarval.VerticalScroller.Gameplay
{
    public class EnemyStatus : ACharacterStatus
    {
        protected override void OnDead()
        {
            GameManager.Instance.IncreaseScore(scoreValue);

            //TODO: Add VFX & SFX
            gameObject.SetActive(false);
        }
    }
}
