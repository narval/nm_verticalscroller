using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay
{    public abstract class ACharacterStatus : MonoBehaviour
    {
        [Header("References")]
        [NoNull][SerializeField] private Collisionable collisionable;

        [Header("Config")]
        [NoNull][SerializeField] protected CharacterConfigSO initialConfig;

        [Header("Custom")]
        [SerializeField] protected int hp;
        [SerializeField] protected int maxHP;
        [SerializeField] protected int scoreValue;

        public virtual int HP
        {
            get => hp;
            set
            {
                hp = Mathf.Max(0, value);
                if (hp == 0)
                {
                    OnDead();
                    
                }
            }
        }

        //Returns a value between 0 and 1 according to the current HP and the maximum HP
        protected float HPPercentage
        {
            get
            {
                return (float)hp / maxHP;
            }
        }

        public  virtual void Start()
        {
              scoreValue = initialConfig.scoreValue;
            maxHP = initialConfig.maxHp;
            hp = maxHP;
            collisionable.OnHitEvent += OnHit;
        }

        private void OnDestroy()
        {
            collisionable.OnHitEvent -= OnHit;
        }

        private void OnEnable()
        {
            collisionable.OnHitEvent += OnHit;
        }

        private void OnDisable()
        {
            collisionable.OnHitEvent -= OnHit;
        }

        protected void OnHit(int damage)
        {
            HP -= damage;
            //Add VFX
        }

        protected abstract void OnDead();

        public void Recover()
        {
            HP = maxHP;
        }
    }
}
