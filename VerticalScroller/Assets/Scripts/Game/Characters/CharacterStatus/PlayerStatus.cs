﻿using UnityEngine;
using Inarval.VerticalScroller.Utils;


namespace Inarval.VerticalScroller.Gameplay
{
    public class PlayerStatus : ACharacterStatus
    {
        [Header("Events")]
        [NoNull][SerializeField] private ASOEvent1<float> hpChangeEvent;

        public override int HP
        {
            get => hp;
            set
            {
                hp = Mathf.Max(0, value);
                hpChangeEvent.Invoke(HPPercentage);
                if (hp == 0)
                {
                    OnDead();

                }
            }
        }

        protected override void OnDead()
        {
            GameManager.Instance.OnPlayerKilled();
        }
    }
}
