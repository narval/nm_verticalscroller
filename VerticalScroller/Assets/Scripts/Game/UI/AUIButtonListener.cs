﻿using UnityEngine;
using UnityEngine.UI;

namespace Inarval.VerticalScroller.Gameplay.UI
{
    public abstract class AUIButtonListener : MonoBehaviour
    {
        [SerializeField] protected Button button;

        protected virtual void Awake()
        {
            button.onClick.AddListener(ButtonClicked);
        }

        protected abstract void ButtonClicked();

    }
}
