using System.Collections.Generic;
using UnityEngine;
using Inarval.VerticalScroller.Utils;
using System.Threading.Tasks;

namespace Inarval.VerticalScroller.Gameplay.UI
{
    public class UILivesManager : MonoBehaviour
    {
        [Header("References")]
        [NoNull] [SerializeField] protected GeneralConfigSO initialConfig;
        [NoNull] [SerializeField] private Transform container;

        [Header("Events")]
        [NoNull][SerializeField] private ASOEvent1<int> currentLivesChangeEvent;

        [Header("Instancers")]
        [NoNull] [SerializeField] private AGameObjectInstancer livesInstancer;

        private List<GameObject> livesList;
        private int currentLives;
        private int maxLives;

        protected void Start()
        {
            currentLivesChangeEvent.ValueEvent += OnCurrentLivesChange;

            livesList = new List<GameObject>();

            maxLives = initialConfig.maxLives;
            currentLives = initialConfig.initiaLives;

            SetInitialLives(maxLives, currentLives);
        }

        protected void OnDestroy()
        {
            currentLivesChangeEvent.ValueEvent -= OnCurrentLivesChange;
        }

        protected void OnEnable()
        {
            currentLivesChangeEvent.ValueEvent += OnCurrentLivesChange;
        }

        protected void OnDisable()
        {
            currentLivesChangeEvent.ValueEvent -= OnCurrentLivesChange;
        }

        protected void OnCurrentLivesChange(int value)
        {
            currentLives = value;
            for (int i = 0; i < livesList.Count; i++)
            {
                livesList[i].SetActive(i <= value-1);
            }   
        }

        async protected void SetInitialLives (int maxLives, int currentLives)
        {
            for (int i = 0; i < maxLives; i++)
            {
                livesInstancer.CreateGameObject(
                    (GameObject go) =>
                    {
                        livesList.Add(go);
                        go.SetActive(false);
                        go.transform.parent = container;
                    }
                    );
            }
            while (livesList.Count != maxLives)
            {
                await Task.Yield();
            }

            OnCurrentLivesChange(currentLives);
        }
    }
}
