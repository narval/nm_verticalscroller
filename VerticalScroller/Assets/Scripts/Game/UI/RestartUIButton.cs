namespace Inarval.VerticalScroller.Gameplay.UI
{
    public class RestartUIButton : AUIButtonListener
    {
        protected override void ButtonClicked ()
        {
            //Button effects may be added here
            GameManager.Instance.RestartGame();
        }
    }
}
