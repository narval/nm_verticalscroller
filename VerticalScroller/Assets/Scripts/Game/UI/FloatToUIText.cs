using UnityEngine;
using TMPro;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay.UI
{
    public class FloatToUIText : ASOEvent1Listener<float>
    {
        [NoNull] [SerializeField] private TMP_Text text;

        protected override void OnValueChange(float value)
        {
            text.text = value.ToString();
        }
    }
}
