using Inarval.VerticalScroller.Gameplay.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay.UI
{
    public class GameOverUIPanel : AEventSOListener
    {
        [Header("References")]
        [NoNull] [SerializeField] private GameObject gameOverPanel;

        [Header("SOEvents")]
        [NoNull] [SerializeField] private SOEvent restartEvent;

        protected override void Start()
        {
            base.Start();
            restartEvent.Event += ClosePanel;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            restartEvent.Event += ClosePanel;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            restartEvent.Event -= ClosePanel;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            restartEvent.Event -= ClosePanel;
        }

        protected override void OnEvent()
        {
            gameOverPanel.SetActive(true);
        }

        protected void ClosePanel ()
        {
            gameOverPanel.SetActive(false);
        }
    }
}
