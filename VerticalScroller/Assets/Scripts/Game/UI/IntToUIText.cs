using UnityEngine;
using TMPro;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay.UI
{
    public class IntToUIText : ASOEvent1Listener<int>
    {
        [NoNull] [SerializeField] private TMP_Text text;

        protected override void OnValueChange(int value)
        {
            text.text = value.ToString();
        }
    }
}
