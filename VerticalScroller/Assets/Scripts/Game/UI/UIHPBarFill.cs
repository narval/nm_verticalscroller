using UnityEngine;
using UnityEngine.UI;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay.UI
{
    public class UIHPBarFill : ASOEvent1Listener<float>
    {
        [NoNull] [SerializeField] private Image fillImage;

        protected override void OnValueChange(float value)
        {
            fillImage.fillAmount = value;
        }
    }
}
