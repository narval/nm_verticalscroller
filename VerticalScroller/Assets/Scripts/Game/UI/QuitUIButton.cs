namespace Inarval.VerticalScroller.Gameplay.UI
{
    public class QuitUIButton : AUIButtonListener
    {
        protected override void ButtonClicked ()
        {
            //Button effects may be added here
            GameManager.Instance.QuitGame();
        }
    }
}
