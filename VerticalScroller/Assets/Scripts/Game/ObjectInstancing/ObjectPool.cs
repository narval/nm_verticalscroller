using System.Collections.Generic;
using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    [System.Serializable]
    public class ObjectPool : MonoBehaviour
    {
        protected List<GameObject> pool;

        public void Init(AGameObjectInstancer instancer, int objectNumber, Transform container)
        {
            pool = new List<GameObject>();
            for (int i = 0; i < objectNumber; i++)
            {
                instancer.CreateGameObject(
                    (GameObject go) =>
                    {
                        pool.Add(go);
                        go.SetActive(false);
                        if (container != null)
                        {
                            go.transform.parent = container;
                        }
                    }
                    );
            }
        }

        public bool HaveAvailableObjects()
        {
            foreach (var item in pool)
            {
                if (!item.activeSelf)
                {
                    return true;
                }
            }
            return false;
        }

        public GameObject GetObject ()
        {
            for (int i = 0; i < pool.Count; i++)
            {
                GameObject go = pool[i];
                if (!go.activeSelf)
                {
                    go.SetActive(true);
                    return go;
                }
            }
            return null;
        }

        public void DisposeAll()
        {
            foreach (var item in pool)
            {
                item.SetActive(false);
            }
        }
    }
}
