﻿namespace Inarval.VerticalScroller.Gameplay
{
    [System.Serializable]
    public struct PoolConfig
    {
        public ObjectPoolManager.PoolType type;
        public int objectNumber;
        public AGameObjectInstancer instancer;
    }
}
