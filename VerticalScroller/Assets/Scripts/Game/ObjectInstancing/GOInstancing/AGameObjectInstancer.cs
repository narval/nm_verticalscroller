using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Inarval.VerticalScroller.Gameplay
{
    /// <summary>
    /// The instancers allows to separate the instantiating logic from the Pool
    /// It allows instantiating objects in various ways like using Addressable assets
    /// </summary>
    public abstract class AGameObjectInstancer : ScriptableObject
    {
        protected System.Action<GameObject> callback;

        public abstract void CreateGameObject(System.Action<GameObject> callback);

        protected abstract void OnAddressableInstantiated(AsyncOperationHandle<GameObject> handle);
    }
}
