﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Inarval.VerticalScroller.Gameplay
{
    [CreateAssetMenu(fileName = "AddressableGameObjectInstancer", menuName = "Utils/GOInstancers/AddressableGameObjectInstancer")]
    public class AddressableGameObjectInstancer : AGameObjectInstancer
    {
        [SerializeField] private AssetReferenceGameObject referenceGameObject;

        public override void CreateGameObject(System.Action<GameObject> callback)
        {
            this.callback = callback;
            referenceGameObject.InstantiateAsync().Completed += OnAddressableInstantiated;
        }

        protected override void OnAddressableInstantiated(AsyncOperationHandle<GameObject> handle)
        {
            handle.Completed -= OnAddressableInstantiated;
            if (handle.Status == AsyncOperationStatus.Succeeded)
            {
                callback?.Invoke(handle.Result);
            }
        }
    }
}
