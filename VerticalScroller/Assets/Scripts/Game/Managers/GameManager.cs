using UnityEngine;
using Inarval.VerticalScroller.Utils;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Inarval.VerticalScroller.Gameplay
{
    public class GameManager : ASingletonManager<GameManager>
    {
        [Header("Instancers")]
        [NoNull] [SerializeField] protected AGameObjectInstancer playerInstancer;
        [NoNull] [SerializeField] protected AGameObjectInstancer canvasInstancer;

        [Header("Configs")]
        [NoNull] [SerializeField] protected GeneralConfigSO initialConfig;

        [Header("Custom")]
        [SerializeField] private int currentScore;
        [SerializeField] private int highScore;
        [SerializeField] protected int currentLives;
        [SerializeField] protected int maxLives;
        [NoNull] [SerializeField] protected List<AWinConditionSO> winConditions;

        [Header("SOEvents")]
        [NoNull] [SerializeField] private ASOEvent1<int> scoreChangeEvent;
        [NoNull] [SerializeField] private ASOEvent1<int> highScoreChangeEvent;
        [NoNull] [SerializeField] private ASOEvent1<int> livesChangeEvent;
        [NoNull] [SerializeField] private ASOEvent1<int> maxLivesChangeEvent;
        [NoNull] [SerializeField] private SOEvent gameOverEvent;
        [NoNull] [SerializeField] private SOEvent restartEvent;

        public int CurrentScore => currentScore;

        public PlayerStatus Player { get; private set; }

        private void Start()
        {
            Init();
            restartEvent.Event += Init;
            canvasInstancer.CreateGameObject(null);
        }

        private void OnEnable()
        {
            restartEvent.Event += Init;
        }

        private void OnDisable()
        {
            restartEvent.Event -= Init;
        }

        private void OnDestroy()
        {
            restartEvent.Event -= Init;
        }

        private void Init()
        {
            scoreChangeEvent.Invoke(0); //May change if continuing a game

            highScore = DataManager.Instance.HighScore;
            highScoreChangeEvent.Invoke(highScore);

            maxLives = initialConfig.maxLives;
            currentLives = initialConfig.initiaLives;
            currentScore = 0;

            if (Player == null)
            {
                Player = FindObjectOfType<PlayerStatus>();
                Player.transform.position = initialConfig.playerInitialPosition;
            }
            else
            {
                RespawnPlayer();
            }        
        }

        async private void RespawnPlayer()
        {
            Player.gameObject.SetActive(false);
            Player.transform.position = initialConfig.playerInitialPosition;

            await Task.Delay(initialConfig.respawnDelayMilliseconds);
            Player.gameObject.SetActive(true);
            Player.Recover();

        }

        public void OnPlayerKilled ()
        {
            currentLives--;
            livesChangeEvent.Invoke(currentLives);

            if (currentLives == 0)
            {
                GameOver();
            }
            else
            {
                RespawnPlayer();
            }
        }

        public void GameOver ()
        {
            DataManager.Instance.SaveData();
            Player.gameObject.SetActive(false);
            gameOverEvent.Invoke();
        }

        public void IncreaseScore (int value)
        {
            currentScore += value;
            if (currentScore > highScore)
            {
                //New High Score
                highScore = currentScore;
                highScoreChangeEvent.Invoke(highScore);

                DataManager.Instance.HighScore = highScore;
            }

            scoreChangeEvent.Invoke(currentScore);
        }

        private void Update()
        {
            CheckWinCondition();

            //Used for debug
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.R))
            {
                GameOver();
            }
#endif
        }

        private void CheckWinCondition()
        {
            foreach (var item in winConditions)
            {
                if (item.CheckCondition())
                {
                    //Win!
                    GameOver(); //TODO: create a win screen
                }
            }
        }

        public void RestartGame ()
        {
            restartEvent.Invoke();
        }

        public void QuitGame ()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
            Application.Quit();
#endif
        }
    }
}
