using System.Collections.Generic;
using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay
{
    public class ObjectPoolManager : ASingletonManager<ObjectPoolManager>
    {
        [Header("References")]
        [SerializeField] private Transform container;

        [Header("Configs")]
        [NoNull] [SerializeField] private ObjectPoolManagerConfigSO config;

        [Header("SOEvents")]
        [NoNull] [SerializeField] private SOEvent restartEvent;        

        private Dictionary<PoolType, ObjectPool> pools;

        public enum PoolType { PlayerBullet , Enemy1, EnemyBullet}

        void Start()
        {
            pools = new Dictionary<PoolType, ObjectPool>();

            foreach (var item in config.objectPools)
            {
                GameObject newGO = new GameObject(item.type.ToString(), typeof(ObjectPool));
                newGO.transform.parent = transform;
                ObjectPool newObjectPool = newGO.GetComponent<ObjectPool>();
                newObjectPool.Init(item.instancer, item.objectNumber, newGO.transform);
                pools.Add(item.type, newObjectPool);
            }

            restartEvent.Event += OnRestart;
        }
        private void OnEnable()
        {
            restartEvent.Event += OnRestart;
        }

        private void OnDisable()
        {
            restartEvent.Event -= OnRestart;
        }

        private void OnDestroy()
        {
            restartEvent.Event -= OnRestart;
        }

        public bool HaveAvailableObjects(PoolType type)
        {
            if (pools.TryGetValue(type, out ObjectPool objectPool))
            {
                return objectPool.HaveAvailableObjects();
            }
            else
            {
                throw new System.Exception("Type not added to ObjectPoolManager configuration");
            }
        }

        public GameObject GetObject(PoolType type)
        {
            if (pools.TryGetValue(type, out ObjectPool objectPool))
            {
                return objectPool.GetObject();
            }
            else
            {
                throw new System.Exception("Type not added to ObjectPoolManager configuration");
            }
        }

        private void OnRestart ()
        {
            foreach (KeyValuePair<PoolType, ObjectPool> entry in pools)
            {
                entry.Value.DisposeAll();
            }
        }
    }
}
