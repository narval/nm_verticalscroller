using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay
{
    public class EnemyManager : ASingletonManager<EnemyManager>
    {
        [Header("Configs")]
        [NoNull] [SerializeField] protected GeneralConfigSO initialConfig;

        [Header("SOEvents")]
        [NoNull] [SerializeField] private ASOEvent1<int> currentTimeSecond;
        [NoNull] [SerializeField] private SOEvent restartEvent;

        [Header("Custom")]
        [SerializeField] ObjectPoolManager.PoolType objectType; //TODO: Waves should be later defined in other config object
        [ReadOnly][SerializeField]  private float nextWaveTimeMark;

        public int CurrentRound { get; private set; }

        private void Start()
        {
            currentTimeSecond.ValueEvent += OnSecondChanges;
            restartEvent.Event += OnRestart;
            nextWaveTimeMark = initialConfig.initialWaveDelay;
            CurrentRound = 0;
        }

        protected void OnDestroy()
        {
            currentTimeSecond.ValueEvent -= OnSecondChanges;
            restartEvent.Event -= OnRestart;
        }

        protected void OnEnable()
        {
            currentTimeSecond.ValueEvent += OnSecondChanges;
            restartEvent.Event += OnRestart;
        }

        protected void OnDisable()
        {
            currentTimeSecond.ValueEvent -= OnSecondChanges;
            restartEvent.Event -= OnRestart;
        }

        private void OnSecondChanges(int second)
        {
            if (second >= nextWaveTimeMark)
            {
                SpawnWave();                
            }
        }

        private void SpawnWave ()
        {
            for (int i = 0; i < initialConfig.enemiesPerWave; i++)
            {
                if (ObjectPoolManager.Instance.HaveAvailableObjects(objectType))
                {
                    SpawnEnemy();
                }
                else
                {
                    //Not enough instances in pool, should destroy current enemies, extend pool or wait
                }
            }
            nextWaveTimeMark += initialConfig.waveDelay;
        }

        private void SpawnEnemy ()
        {
            GameObject enemyGO = ObjectPoolManager.Instance.GetObject(objectType);
            EnemyStatus enemy = enemyGO.GetComponent<EnemyStatus>();
            enemy.Start();
            enemy.Recover();
            enemyGO.transform.position = BorderManager.Instance.GetRandomEnemySpawnPosition();
        }

        private void OnRestart()
        {
            nextWaveTimeMark = initialConfig.initialWaveDelay;
            CurrentRound = 0;
        }
    }
}
