﻿using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    [CreateAssetMenu(fileName = "RoundConditionSO", menuName = "Configurations/WinConditions/RoundConditionSO")]
    public class RoundConditionSO : AWinConditionSO
    {
        [SerializeField] private int round;

        public override bool CheckCondition()
        {
            return EnemyManager.Instance.CurrentRound >= round;
        }
    }

}
