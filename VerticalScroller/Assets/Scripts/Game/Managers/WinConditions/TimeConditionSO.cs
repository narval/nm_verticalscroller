﻿using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    [CreateAssetMenu(fileName = "TimeConditionSO", menuName = "Configurations/WinConditions/TimeConditionSO")]
    public class TimeConditionSO : AWinConditionSO
    {
        [SerializeField] private int timeInSeconds;

        public override bool CheckCondition()
        {
            return TimeManager.Instance.IntTime >= timeInSeconds;
        }
    }


}
