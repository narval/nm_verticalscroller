﻿using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    public abstract class AWinConditionSO : ScriptableObject
    {
        public abstract bool CheckCondition();
    }
}
