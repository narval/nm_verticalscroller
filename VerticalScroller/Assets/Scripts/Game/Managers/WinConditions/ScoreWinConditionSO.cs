﻿using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    [CreateAssetMenu(fileName = "ScoreWinConditionSO", menuName = "Configurations/WinConditions/ScoreWinConditionSO")]
    public class ScoreWinConditionSO : AWinConditionSO
    {
        [SerializeField] private int score;

        public override bool CheckCondition()
        {
            return GameManager.Instance.CurrentScore >= score;
        }
    }


}
