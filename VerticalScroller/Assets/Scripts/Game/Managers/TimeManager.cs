using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay
{
    public class TimeManager : ASingletonManager<TimeManager>
    {
        [Header("Debug")]
        [SerializeField] private bool pause;
        [SerializeField] private float time;

        [Header("SOEvents")]
        [NoNull] [SerializeField] private ASOEvent1<int> TimeChangeEvent;
        [NoNull] [SerializeField] private SOEvent restartEvent;

        private int lastIntTime;
        public int IntTime
        {
            get => (int)time;

            set
            {
                time = value;
            }
        }

        public float Time
        {
            get => time;
            set
            {
                time = value;
                if (lastIntTime != IntTime)
                {
                    lastIntTime = IntTime;
                    TimeChangeEvent?.Invoke(IntTime);
                }
            }
        }

        public void Reset()
        {
            lastIntTime = -1;
            time = 0;
        }

        void Update()
        {
            if (!pause)
            {
                Time += UnityEngine.Time.deltaTime;

            }
        }

        private void Start()
        {
            restartEvent.Event += OnRestart;
        }

        private void OnEnable()
        {
            restartEvent.Event += OnRestart;
        }

        private void OnDisable()
        {
            restartEvent.Event -= OnRestart;
        }

        private void OnDestroy()
        {
            restartEvent.Event -= OnRestart;
        }

        private void OnRestart()
        {
            Time = 0;
        }
    }
}
