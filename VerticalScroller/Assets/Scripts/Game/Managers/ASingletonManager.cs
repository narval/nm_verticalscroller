﻿using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    public abstract class ASingletonManager<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance;
        public static T Instance {
            get
            {
                if (instance == null)
                {
                    new GameObject("DataManager", typeof(T));
                }
                return instance;
            } 
            private set
            {
                instance = value;
            } 
        }

        protected virtual void Awake()
        {
            Instance = this as T;
        }
    }
}
