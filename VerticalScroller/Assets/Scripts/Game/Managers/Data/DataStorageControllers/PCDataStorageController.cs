﻿#if UNITY_EDITOR || UNITY_STANDALONE
using UnityEngine;
using System.Runtime.InteropServices;

namespace Inarval.VerticalScroller.Data
{
    public class PCDataStorageController : ADataStorageController, ISaveData, ILoadData
    {
        private const string DATA_FILE_NAME = "VSSave";

        private string SaveFilePath => System.IO.Path.Combine(Application.persistentDataPath, DATA_FILE_NAME).Replace("\\", System.IO.Path.DirectorySeparatorChar.ToString());

        public override void LoadData(System.Action<GameSave> onSuccess, System.Action<System.Exception> onError)
        {
            try
            {
                GameSave data;

                if (!System.IO.File.Exists(SaveFilePath))
                {
                    onError?.Invoke(new System.IO.FileNotFoundException());
                    return;
                }
                byte[] serializedData = System.IO.File.ReadAllBytes(SaveFilePath);
                data = DeserializeGameSave(serializedData);

                onSuccess?.Invoke(data);
            }
            catch (System.Exception e)
            {
                onError?.Invoke(e);
            }
        }

        public override void SaveData(GameSave data, System.Action onSuccess, System.Action<System.Exception> onError)
        {
            try
            {
                byte[] serializedData = SerializeGameSave(data);
                string filename = SaveFilePath;

                System.IO.File.WriteAllBytes(filename, serializedData);

                onSuccess?.Invoke();
            }
            catch (System.Exception e)
            {
                onError?.Invoke(e);
            }
            
        }

        //Based on https://stackoverflow.com/questions/3278827/how-to-convert-a-structure-to-a-byte-array-in-c
        //TODO: Should make these generics (but I don't have much time left :/)
        private static byte[] SerializeGameSave (GameSave data)
        {
            int size = Marshal.SizeOf(typeof(GameSave));
            byte[] serializedArray = new byte[size];
            byte[] arr = new byte[size];

            System.IntPtr ptr = System.IntPtr.Zero;
            try
            {
                ptr = Marshal.AllocHGlobal(size);
                Marshal.StructureToPtr(data, ptr, true);
                Marshal.Copy(ptr, arr, 0, size);
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }
            return arr;
        }

        private static GameSave DeserializeGameSave (byte[] bytes)
        {
            GameSave data = new GameSave();

            int size = Marshal.SizeOf(data);
            System.IntPtr ptr = System.IntPtr.Zero;
            try
            {
                ptr = Marshal.AllocHGlobal(size);

                Marshal.Copy(bytes, 0, ptr, size);

                data = (GameSave)Marshal.PtrToStructure(ptr, data.GetType());
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }
            return data;
        }
    }
}
#endif