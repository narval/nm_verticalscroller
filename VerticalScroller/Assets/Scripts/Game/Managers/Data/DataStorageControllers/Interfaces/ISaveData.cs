﻿namespace Inarval.VerticalScroller.Data
{    public interface ISaveData
    {
        public void SaveData(GameSave data, System.Action onSuccess, System.Action<System.Exception> onError);
    }
}
