﻿namespace Inarval.VerticalScroller.Data
{
    public interface ILoadData
    {
        public void LoadData(System.Action<GameSave>onSuccess, System.Action<System.Exception> onError);
    }
}
