namespace Inarval.VerticalScroller.Data
{
    public abstract class ADataStorageController : ISaveData, ILoadData
    {
        public abstract void LoadData(System.Action<GameSave> onSuccess, System.Action<System.Exception> onError);
        public abstract void SaveData(GameSave data, System.Action onSuccess, System.Action<System.Exception> onError);
    }
}
