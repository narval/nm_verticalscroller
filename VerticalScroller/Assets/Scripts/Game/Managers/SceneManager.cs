using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Inarval.VerticalScroller.Utils;

namespace Inarval.VerticalScroller.Gameplay
{
    public class SceneManager : ASingletonManager<SceneManager>
    {
        [Header("Instancers")]
        [NoNull] [SerializeField] protected AGameObjectInstancer playerInstancer;
        [NoNull] [SerializeField] protected AGameObjectInstancer canvasInstancer;
        [NoNull] [SerializeField] protected AGameObjectInstancer managersInstancer;

        protected override  void Awake()
        {
            base.Awake();
            InstancePlayer();
        }

        private void InstancePlayer ()
        {
            playerInstancer.CreateGameObject(InstanceCanvas);

        }

        private void InstanceCanvas(GameObject _)
        {
            canvasInstancer.CreateGameObject(InstanceManagers);

        }

        private void InstanceManagers(GameObject _)
        {
            managersInstancer.CreateGameObject(null);

        }

    }
}
