using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    public class BorderManager : ASingletonManager<BorderManager>
    {
        [Header("References")]
        [SerializeField] private BoxCollider2D topCollider;
        [SerializeField] private BoxCollider2D bottomCollider;
        [SerializeField] private BoxCollider2D rightCollider;
        [SerializeField] private BoxCollider2D leftCollider;

        [Header("Custom")]
        [SerializeField] private float thickness = 1;
        [SerializeField] private float enemyOffsetSpawnPositionY = 1;

        private Camera mainCamera;

        private Vector3 bottomLeft;
        private Vector3 bottomRight;
        private Vector3 topLeft;
        private Vector3 topRight;
        private Vector3 center;

        float height;
        float width;

        private void Start()
        {
            mainCamera = Camera.main;
            SetBounds();
        }

        //Sets the BoxCollider2Ds as borders 
        private void SetBounds ()     
        {        
            //Calculate corners
            bottomLeft = mainCamera.ViewportToWorldPoint(new Vector3(0, 0, mainCamera.nearClipPlane));
            bottomRight = mainCamera.ViewportToWorldPoint(new Vector3(1, 0, mainCamera.nearClipPlane));
            topLeft = mainCamera.ViewportToWorldPoint(new Vector3(0, 1, mainCamera.nearClipPlane));
            topRight = mainCamera.ViewportToWorldPoint(new Vector3(1, 1, mainCamera.nearClipPlane));
            center = mainCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, mainCamera.nearClipPlane));

            //Move the colliders
            topCollider.transform.position = new Vector3(center.x, topRight.y + (thickness * 0.5f), center.z);
            bottomCollider.transform.position = new Vector3(center.x, bottomLeft.y - (thickness * 0.5f), center.z);
            leftCollider.transform.position = new Vector3(bottomLeft.x - (thickness * 0.5f), center.y, center.z);
            rightCollider.transform.position = new Vector3(bottomRight.x + (thickness * 0.5f), center.y, center.z);

            //Calculate the projected dimentions
            height = (topLeft - bottomLeft).magnitude;
            width = (bottomLeft - bottomRight).magnitude;
             
            //Sets the size of the colliders
            topCollider.GetComponent<BoxCollider2D>().size = new Vector2(width, thickness);
            bottomCollider.GetComponent<BoxCollider2D>().size = new Vector2(width, thickness);
            leftCollider.GetComponent<BoxCollider2D>().size = new Vector2(thickness, height);
            rightCollider.GetComponent<BoxCollider2D>().size = new Vector2(thickness, height);
        }

        //Return a point between the side borders 
        public Vector2 GetRandomEnemySpawnPosition ()
        {
            return new Vector2()
            {
                x = Random.Range(topLeft.x, topRight.x),
                y = topLeft.y + enemyOffsetSpawnPositionY,
            };
        }
    }
}
