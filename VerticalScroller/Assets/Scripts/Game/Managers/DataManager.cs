using UnityEngine;
using Inarval.VerticalScroller.Data;

namespace Inarval.VerticalScroller.Gameplay
{
    public class DataManager : ASingletonManager<DataManager>
    {
        private ADataStorageController saveLoadController;
        private GameSave data;

        public int HighScore
        {
            get => data.highScore;
            set
            {
                data.highScore = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            GameObject.DontDestroyOnLoad(this);

#if UNITY_EDITOR
            saveLoadController = new PCDataStorageController();
#elif UNITY_STANDALONE
            saveLoadController = new PCDataStorageController();
#endif
            LoadData();
        }        

        //This should happen before other managers start asking for data
        public void LoadData ()
        {
            saveLoadController.LoadData(
                onSuccess: (GameSave data) =>
                {
                    this.data = data; 
                },
                onError: (System.Exception e) =>
                {
                    if (e is System.IO.FileNotFoundException)
                    {
                        data = new GameSave();
                    }
                    else
                    {
                        throw e;
                    }                    
                }
            );
        }

        public void SaveData ()
        {
            saveLoadController.SaveData(
                data,
                onSuccess: () =>
                {
                    //GameSaved!
                },
                onError: (System.Exception e) =>
                {
                    throw e;
                }
            );
        }

        //TODO: Make a delete data function and add an editor script button
    }
}
