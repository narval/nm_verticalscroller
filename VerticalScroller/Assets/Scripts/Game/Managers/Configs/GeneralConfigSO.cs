using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    [CreateAssetMenu(fileName = "GeneralConfigSO", menuName = "Configurations/General/GeneralConfigSO")]
    public class GeneralConfigSO : ScriptableObject
    {
        [Header("Player")]
        [Min(1)] public int maxLives = 5;
        [Min(1)] public int initiaLives = 2;
        [Min(0f)] public int respawnDelayMilliseconds;

        public Vector3 playerInitialPosition;

        [Header("Enemy Waves")]
        //TODO: Should depend on the used controller
        [Min(0f)] public float initialWaveDelay = 2f; 
        [Min(0f)] public float waveDelay = 10f; 
        [Min(0f)] public float enemiesPerWave = 4f;
    }
}
