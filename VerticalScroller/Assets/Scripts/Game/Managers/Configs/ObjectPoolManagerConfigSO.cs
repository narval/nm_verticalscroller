﻿using System.Collections.Generic;
using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    [CreateAssetMenu(fileName = "ObjectPoolManagerConfigSO", menuName = "Configurations/ObjectPoolManagerConfigSO")]
    public class ObjectPoolManagerConfigSO : ScriptableObject
    {
        public List<PoolConfig> objectPools;
    }
}
