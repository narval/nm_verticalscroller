﻿using UnityEditor;
using UnityEngine;

namespace Inarval.VerticalScroller.Gameplay
{
    [CustomEditor(typeof(GeneralConfigSO))]
    public class GeneralConfigSOEditor : Editor
    {
        private SerializedProperty originPosition;
        private const string POSITION_FIELD_NAME = "playerInitialPosition";

        private void OnEnable()
        {
            originPosition = serializedObject.FindProperty(POSITION_FIELD_NAME);

            SceneView.duringSceneGui += OnSceneGUI;
        }

        void OnDisable()
        {
            SceneView.duringSceneGui -= OnSceneGUI;
        }

        protected virtual void OnSceneGUI(SceneView sceneView)
        {
            serializedObject.Update();

            EditorGUI.BeginChangeCheck();
            Vector3 newPosition = Handles.DoPositionHandle(originPosition.vector3Value, Quaternion.identity);
            if (EditorGUI.EndChangeCheck())
            {
                originPosition.vector3Value = newPosition;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}
