# Isaac López Procopio - VerticalScroller -



## About

This document will cover all the design and implementation decisions I took for the project, as well as all the features I'd have liked to add if having more time.

# Gameplay Instructions

## Playing in Editor

- To play in the Unity editor you only have to run the Game scene.
- The preferred aspect ratio for the game screen is 1:1.

## Playing the build
- There's no special need to play, just run the .Exe inside the Bin folder in the base of the repo.

## Controllers
- W, A, S, D, or arrow keys to move.
- space bar to shoot a bullet.
- The UI buttons should be clicked with the cursor.

# Technical Details
For making the project I decided to prioritize versatility, and modularity and to provide an easy way to customize the game for the game designers. To accomplish this, I used various programming patterns such as observer, strategy, and singleton.

# Package Usage
I only used TextMeshPro and Unity addressables for this project. I tried to relay only in the default unity resources instead of using external libraries.

# Key Managers
The most important scripts are the Managers. These scripts are singletons and provide high-level functionalities and some global game logic. They are not directly referenced but used through their singleton static reference.

Each manager has an individual job:
- The GameManager manages the game's main variables and controls the end of the game conditions.
- The DataManager manages the data loading or saving in persistent memory.
- The TimeManager handles a global timer for every other component to use.
- The EnemyManager Manages the instantiation of the enemies in the scene.
- The BorderManager generates the border of the scene and provides functions regarding the game area.
- The ObjectPoolManager allows to implementation of an Object Pooling pattern to reduce the memory overhead of creating and destroying objects. This is especially useful in games with bullets or any other kind of short-lived objects that are created a lot (as bullets or recurrent enemies).
- The SceneManager that allows to load the other main objects of the game. 

These managers don't contain references to scene GameObjets but events to broadcast information among any interested listener (an observer pattern). The events are shared through ScriptableObjects to these listeners.

# The Canvas
Contains the structure and scripts to display the user interface.
AS there are no interactable objects in the UI, almost all elements are listed as variables to update their graphics. The only exceptions are two buttons in the GameOver panel.

# The Characters
The characters (player and enemies) are prefabs instantiated in the scene in runtime. Each one behaves similarly but with some differences given by their scrips. 

- The input is taken from an interchangeable controller (Strategy pattern). In the case of the player, it is decided based on the current platform and for the enemies, it is set in the script variables. (this should be added to the enemy common configuration stored in a scriptable object)

- The movement is handled by a script that takes the input and applies it into a RigidBody. I decided to use a RigidBody instead of directly moving the transform to take advantage of the border and the enemy collision calculation instead of making them manually.

- The status contains info about the current character's life points and other values related to the collision with other characters.

# The Bullets
The bullets are very simple. They just set a velocity to their RigidBodies and disappear when colliding with any enemy or border.

## Implementation
As I said before. I used many resources to link every part of the game components but my preferred choice was the use of ScriptableObjects for sharing global information and events. I tried to separate the logic in controllers so they could be changed at any time in the game. This may be applied to upgrades or changes in enemy behaviors.

I made the script inspectors easy to read using attributes in the variables and added custom editors for some classes.

All assets are loaded asynchronously using Unity's Addressable system but this can be easily replaced by any other instantiating method such as GameObject.Instantiate just by adding a new instantiator controller.

# The challenges
I faced several challenges during the implementation of the project. One of them was to accomplish the requisite of loading the assets asynchronously. This gave me errors when building for PC because some of the scriptable assets were not referencing the same assets.

I probably over-engineered some of the systems. I normally prefer a more dynamic approach to game development and make some prototypes and test different but fast approaches to what I intend. But this time I focused on making everything as generic and reusable as I could, even if I couldn't give more time to things I like to do more (such as making a good IA for enemies).

Other than that, I'm in the middle of a relocation and also working in the day, so I spent as much time as I could, but there were many distractions and less time than I would like to have.

# Current status
The game is working as intended. Even if there are many things to improve or correct, I'm happy with the result.

# What would I change
A lot of things:
- I'd have added a SetupScene and a Loading Scene (and load the game scene additively).
- I make a better way to handle loading priority. It was too late when I realized how important it was when loading everything async.
- I'd have implemented a Delete save function and an easy way to call it from the editor.
- I'd add assemblies to speed up the compilation time.
- I'd have made more and more complex IA for the enemies. I've probably used a finite-state machine.
- I've added more useful editor scripts and utilities.
- I've made sure all the customizable elements are in the ScriptableObjects.
- I've added some sounds, music, and VFX.

# The strong points
I'm especially confident in the way I made some systems. I think they are pretty robust and allow easy customization and expansion. One of the ones I'm most proud of is the Save/Load system. I've been dealing with this a lot in the last years as a port developer and having a way to easily define controllers for various platforms or environments is very important for multiplatform development.





# The next steps
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.



